from raik_dashboard.layouts import sample_layout
from raik_dashboard.logic.layouts import add_layout_module, get_layouts


def test_add_layout_module__round_trip():
    """
    A simple test that adds the sample layout module and check it is
    listed in retrieval
    """
    add_layout_module("raik_dashboard.layouts")

    assert {"raik_dashboard.sample_layout": sample_layout} == get_layouts()
