from raik_dashboard.layouts.base_element import BaseElement


def test_class_or_init_attr__init_arg_set():
    x = BaseElement(heading="Trial")

    assert "Trial" == x.heading


def test_class_or_init_attr__class_var_set():
    # noinspection PyAbstractClass
    class T(BaseElement):
        heading = "Trial 2"

    x = T()

    assert "Trial 2" == x.heading

    # Check this is an instance variable

    T.heading = "B"

    assert "Trial 2" == x.heading


def test_class_or_init_attr__class_var_set_to_falsey_value():
    # noinspection PyAbstractClass
    class T(BaseElement):
        heading = ""

    x = T()

    assert "" == x.heading
