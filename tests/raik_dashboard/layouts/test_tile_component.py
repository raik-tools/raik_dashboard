"""
This file focuses on component testing of the tile and related components.
"""
from raik_dashboard.layouts import Tile, TextElement
from raik_dashboard.layouts.base_element import BaseElement


def test_two_level_layout__spot_check_structure():
    """
    This performs a spot check on the interface to send to the front end.
    """
    simple_layout = Tile(
        Tile(TextElement(), TextElement(), vertical=True),
        Tile(TextElement(), TextElement()),
    )()

    assert "text" == simple_layout["children"][0]["children"][0]["type"]
    assert 0 < simple_layout["children"][1]["children"][1]["data_id"]
    assert simple_layout["vertical"] is False
    assert simple_layout["children"][0]["vertical"] is True


def test_two_level_layout__element_ids_captured():
    """
    Check that the text elements below are globally accessible
    """
    text_1 = TextElement()
    text_2 = TextElement()
    text_3 = TextElement()
    text_4 = TextElement()
    Tile(
        Tile(text_1, text_2, vertical=True),
        Tile(text_3, text_4),
    )()

    assert text_1.id != text_2.id
    assert text_1 == BaseElement.get_elements()[text_1.id]
    assert text_2 == BaseElement.get_elements()[text_2.id]
    assert text_3 == BaseElement.get_elements()[text_3.id]
    assert text_4 == BaseElement.get_elements()[text_4.id]
