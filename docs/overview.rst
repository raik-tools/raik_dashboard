========
Overview
========

Scope
=====

This project includes the redistributable python modules raik_dashboard and raik_dashboard_site. The
rest of the files in this project are used as an example on how to set up a dashboard site.


Usage
=====


Concepts
========

Abnormal Situation Management
-----------------------------

I only have limited time to develop this but I also want to keep with
some of the principles of ASM from my past life.  One of the simplest
ways of achieving ASM is the "stand back" test.  That is, if nothing
pops out on the display when standing a distance back, then everything
is okay.  As abnormal situations occur (reflected in state) then areas
on the dashboard that are important will begin to stand out.


Code-only Configuration
-----------------------

The concept behind this dashboard application is that all configuration is
performed in code.  The target audience will be people who are familiar with
coding and can appropriate script their deployments and updates.  The intended
use case is to create a dashboard with limited functionality quickly and that
it will remain stable and secure.

