"""
This script is to help calculate ranges of colours.  It is a prototyping script
and not intended for regular use.

References for contrast approximations:

https://en.wikipedia.org/wiki/HSL_and_HSV

Summary:

* HSV and HSL better mimic eye perception
* HSL:  100% L is white
* HSV:  100% V is shining white light on coloured object
* YIQ:  This might be a better option, adjusting Y

"""
# This requires dev dependencies
import colorsys
import pathlib

from PIL import Image, ImageDraw, ImageFont


def create_colour_samples(distance=0.01):
    """
    This function creates a single array of samples at the specified distance apart.
    """
    colour_r = 0
    while colour_r <= 1.0:
        colour_g = 0
        while colour_g <= 1.0:
            colour_b = 0
            while colour_b <= 1.0:
                yield colour_r, colour_g, colour_b
                colour_b += distance
            colour_g += distance
        colour_r += distance


def rgb_string_to_tuple(col):
    r = int(col[1:3], 16) / 255
    g = int(col[3:5], 16) / 255
    b = int(col[5:7], 16) / 255
    return r, g, b


def tuple_to_rgb_string(col):
    r = int(col[0] * 255)
    g = int(col[1] * 255)
    b = int(col[2] * 255)
    return f"#{r:02x}{g:02x}{b:02x}"


SUBJECTIVE_RED = 0.299
SUBJECTIVE_GREEN = 0.587
SUBJECTIVE_BLUE = 0.114


def subjective_brightness(rgb: tuple) -> int:
    """
    Brightness Approximation
    https://stackoverflow.com/questions/9733288/how-to-programmatically-calculate-the-contrast-ratio-between-two-colors
    """
    r, g, b = rgb
    return SUBJECTIVE_RED * r + SUBJECTIVE_GREEN * g + SUBJECTIVE_BLUE * b


def relative_luminance(rgb: tuple) -> float:
    """
    https://www.w3.org/TR/WCAG20/#relativeluminancedef

    0 is dark
    1 is light
    """
    processed_rgb = list(rgb)
    for i, v in enumerate(processed_rgb):
        if processed_rgb[i] < 0.03928:
            processed_rgb[i] = processed_rgb[i] / 12.92
        else:
            processed_rgb[i] = ((processed_rgb[i] + 0.055) / 1.055) ** 2.4

    return (
        0.2126 * processed_rgb[0]
        + 0.7152 * processed_rgb[1]
        + 0.0722 * processed_rgb[2]
    )


def contrast_ratio(rgb_a: tuple, rgb_b: tuple):
    """
    https://www.accessibility-developer-guide.com/knowledge/colours-and-contrast/how-to-calculate/
    https://www.w3.org/TR/WCAG20/#contrast-ratiodef

    Always > 1
    """
    l1 = relative_luminance(rgb_a)
    l2 = relative_luminance(rgb_b)
    return (max(l1, l2) + 0.05) / (min(l1, l2) + 0.05)


def a_is_lighter_than_b(rgb_a: tuple, rgb_b: tuple):
    """
    https://www.accessibility-developer-guide.com/knowledge/colours-and-contrast/how-to-calculate/
    https://www.w3.org/TR/WCAG20/#contrast-ratiodef

    Always > 1
    """
    l1 = relative_luminance(rgb_a)
    l2 = relative_luminance(rgb_b)
    return l1 > l2


def brighten_colour(rgb: tuple, amount) -> tuple:
    """
    Moving Y Channel for a better perceptual increase.  Amount is 0 -> 1
    """
    yiq = colorsys.rgb_to_yiq(*rgb)
    return colorsys.yiq_to_rgb(
        yiq[0] + amount,
        yiq[1],
        yiq[2],
    )


def darken_colour(rgb: tuple, amount) -> tuple:
    """
    Moving Y Channel for a better perceptual increase.  Amount is 0 -> 1
    """
    yiq = colorsys.rgb_to_yiq(*rgb)
    return colorsys.yiq_to_rgb(
        yiq[0] - amount,
        yiq[1],
        yiq[2],
    )


def estimate_required_luminance_shift(
    dark_colour, light_colour, target_contrast, adjust_light_colour=True
):
    """
    This aims to achieve a target contrast in essentially a reverse
    operation to the contrast calc.  This is an estimate to apply to
    a Y channel of the intended light colour.
    """
    estimate_factor = 0.5
    # initial_contrast = contrast_ratio(ref_colour, adj_colour)
    l_d = relative_luminance(dark_colour)
    l_l = relative_luminance(light_colour)
    # if l_l < l_d:
    #     l_target = target_contrast * (l_d + 0.05) - 0.05
    # else:
    #     l_target = (l_l + 0.05) / target_contrast - 0.05

    if adjust_light_colour:
        l_target = target_contrast * (l_d + 0.05) - 0.05
        return estimate_factor * (l_target - l_l)
    else:
        l_target = (l_l + 0.05) / target_contrast - 0.05
        return estimate_factor * (l_target - l_d)


def adjust_contrast(
    ref_colour: tuple, adj_colour: tuple, target_contrast_ratio: float, dark=True
) -> tuple:
    """
    Keeps the hue but changes the contrast.  This is not a tidy code and with
    time could be more declarative in its processing.

    :param ref_colour: A constant colour. A python RGB colour tuple with each value 0.0 to 1.0
    :param adj_colour: A colour to change. A python RGB colour tuple with each value 0.0 to 1.0
    :param target_contrast_ratio: A value >1.0
    :param dark: The adjusted colour is darker than the reference colour
    """
    precision = 0.005
    adj_yiq = colorsys.rgb_to_yiq(*adj_colour)

    # As a starting point, set to the same Y value and
    # estimate using a luminance adjustment
    initial_contrast = contrast_ratio(ref_colour, adj_colour)

    if dark:
        initial_adjustment = estimate_required_luminance_shift(
            colorsys.yiq_to_rgb(*adj_yiq),
            ref_colour,
            target_contrast_ratio,
            adjust_light_colour=False,
        )
    else:
        initial_adjustment = estimate_required_luminance_shift(
            ref_colour,
            colorsys.yiq_to_rgb(*adj_yiq),
            target_contrast_ratio,
            adjust_light_colour=True,
        )

    ret_colour = colorsys.yiq_to_rgb(
        adj_yiq[0] + initial_adjustment,
        adj_yiq[1],
        adj_yiq[2],
    )

    # Not all hue combinations result in such straight forward adjustments.
    # This loop will make incremental adjustments.
    after_contrast = contrast_ratio(ref_colour, ret_colour)
    abs_shift = 0
    for _ in range(100):
        after_contrast = contrast_ratio(ref_colour, ret_colour)
        if abs(after_contrast - target_contrast_ratio) > precision:
            if dark:
                shift_y = estimate_required_luminance_shift(
                    ret_colour,
                    ref_colour,
                    target_contrast_ratio,
                    adjust_light_colour=False,
                )
            else:
                shift_y = estimate_required_luminance_shift(
                    ref_colour,
                    ret_colour,
                    target_contrast_ratio,
                    adjust_light_colour=True,
                )

            abs_shift += shift_y
            ret_colour = colorsys.yiq_to_rgb(
                adj_yiq[0] + initial_adjustment + abs_shift,
                adj_yiq[1],
                adj_yiq[2],
            )
        else:
            break

    print(
        f"Colour Adjust:  Before {initial_contrast:0.2f} vs After {abs(after_contrast):0.2f} "
        f"(Target {target_contrast_ratio})"
    )

    return ret_colour


def output_colour_samples(samples, swash_size=10, file="output_colour_samples.bmp"):
    sample_count = len(samples)
    width = swash_size
    height = sample_count * swash_size
    image = Image.new(mode="RGB", size=(width, height))
    draw = ImageDraw.Draw(image)
    sample_pos = 0

    for r, g, b in samples:
        col = (int(r * 255), int(g * 255), int(b * 255))
        y_top = sample_pos * swash_size
        draw.rectangle(
            xy=(0, y_top, swash_size, y_top + swash_size),
            fill=col,
            outline=None,
            width=0,
        )
        sample_pos += 1

    image.save(file)


def output_overlay_samples(
    bg_samples,
    fg_samples,
    swash_size=10,
    font_size=8,
    fg_letter="A",
    file="output_overlay_samples.bmp",
):
    bg_sample_count = len(bg_samples)
    fg_sample_count = len(fg_samples[0])
    width = fg_sample_count * swash_size
    height = bg_sample_count * swash_size
    image = Image.new(mode="RGB", size=(width, height))
    draw = ImageDraw.Draw(image)

    font_path = (
        pathlib.Path(__file__).parent.parent
        / "raik_dashboard/static/raik_dashboard/vendor/b612_font/ttf/B612-Regular.ttf"
    )
    # https://blog.gimm.io/difference-between-pixel-px-and-point-pt-font-sizes-in-email-signatures/
    font = ImageFont.truetype(str(font_path.absolute()), int(font_size * 1.333))

    csv_data = []

    for bg_i, bg_rgb in enumerate(bg_samples):
        bg_col = (int(bg_rgb[0] * 255), int(bg_rgb[1] * 255), int(bg_rgb[2] * 255))
        row_data = []
        csv_data.append(row_data)
        for fg_i, fg_rgb in enumerate(fg_samples[bg_i]):
            fg_col = (int(fg_rgb[0] * 255), int(fg_rgb[1] * 255), int(fg_rgb[2] * 255))

            x_left = fg_i * swash_size
            y_top = bg_i * swash_size
            draw.rectangle(
                xy=(x_left, y_top, x_left + swash_size, y_top + swash_size),
                fill=bg_col,
                outline=None,
                width=0,
            )
            draw.text((x_left + 2, y_top + 2), fg_letter, font=font, fill=fg_col)

            row_data.append(
                f"({tuple_to_rgb_string(bg_rgb)})"
                f"({tuple_to_rgb_string(fg_rgb)}):"
                f"{contrast_ratio(bg_rgb, fg_rgb):0.3f}"
            )

    image.save(file)
    with open(f"{file}.csv", "w") as txt:
        for row in csv_data:
            txt.write(", ".join(row))
            txt.write("\n")


def main_1():
    """
    This function creates a plain colour sample using the tools above
    """
    samples = list(create_colour_samples(distance=0.2))
    output_colour_samples(samples, file="main_1.bmp")


def generate_bright_scheme_1(bg_colour, fg_references):
    """
    This code uses a base colour and generates a colour map
    based on relevant contrast.  It also overlays foreground samples
    onto that colour map for comparison.

    This script is being used to determine the minimum contrast
    proportion for the target colour scheme as well as finding
    appropriate colours for primary, info, success, warning, danger

    Starting point:  https://colorkit.co/palettes/

    """
    contrast_proportion = 0.1

    samples = [
        darken_colour(bg_colour, 5 * contrast_proportion),
        darken_colour(bg_colour, 4 * contrast_proportion),
        darken_colour(bg_colour, 3 * contrast_proportion),
        darken_colour(bg_colour, 2 * contrast_proportion),
        darken_colour(bg_colour, 1 * contrast_proportion),
        bg_colour,
        brighten_colour(bg_colour, 1 * contrast_proportion),
        brighten_colour(bg_colour, 2 * contrast_proportion),
        brighten_colour(bg_colour, 3 * contrast_proportion),
        brighten_colour(bg_colour, 4 * contrast_proportion),
        brighten_colour(bg_colour, 5 * contrast_proportion),
    ]
    for i, s in enumerate(samples):
        print(f"{i: 3}: {s} {subjective_brightness(s)}")

    output_colour_samples(
        samples,
        file="generate_bright_scheme_1_linear.bmp",
    )
    output_overlay_samples(
        bg_samples=samples,
        fg_samples=[fg_references] * len(samples),
        swash_size=20,
        font_size=14,
    )


def generate_calculated_fg_scheme(bg_colour, fg_options):
    """
    This code uses the given background reference colour and produces
    a matrix of colour options aiming to optimise the contrast ratios.

    The first row of samples are the unaltered fg_samples.

    Each row after provide an option of low, med, and high contrast while
    aiming to preserve the hue.

    This keeps the bg colour constant.
    """
    # H = 0:  RED
    # H = 33 / 120Deg:  GREEN
    # H = 33 / 120Deg:  GREEN
    # H = 67 / 240Deg:  BLUE
    fg_samples = [fg_options]

    for contrast, brighten in [
        (7.0, True),
        (4.5, True),
        (3.0, True),
        (2.0, True),
        (1.5, True),
        (1.2, True),
        (1.1, True),
        (1.0, True),
        (1.1, False),
        (1.2, False),
        (1.5, False),
        (2.0, False),
        (3.0, False),
        (4.5, False),
        (7.0, False),
    ]:
        fg_samples.append(
            [adjust_contrast(bg_colour, x, contrast, brighten) for x in fg_options]
        )

    output_overlay_samples(
        bg_samples=[bg_colour] * len(fg_samples),
        fg_samples=fg_samples,
        swash_size=20,
        font_size=14,
        file="generate_calculated_fg_scheme.bmp",
    )


if __name__ == "__main__":
    main_1()

    # Colour References
    base_colour = rgb_string_to_tuple("#d9d0b4")
    primary_reference = rgb_string_to_tuple("#879e82")
    light_reference = rgb_string_to_tuple("#7d6b57")
    bold_reference = rgb_string_to_tuple("#666b5e")
    info_reference = rgb_string_to_tuple("#057dcd")
    success_reference = rgb_string_to_tuple("#007f4e")
    warning_reference = rgb_string_to_tuple("#f37324")
    danger_reference = rgb_string_to_tuple("#e12729")

    overlay_samples = [
        primary_reference,
        light_reference,
        bold_reference,
        info_reference,
        success_reference,
        warning_reference,
        danger_reference,
    ]
    # H = 0:  RED
    # H = 33 / 120Deg:  GREEN
    # H = 33 / 120Deg:  GREEN
    # H = 67 / 240Deg:  BLUE

    generate_bright_scheme_1(bg_colour=base_colour, fg_references=overlay_samples)
    generate_calculated_fg_scheme(bg_colour=base_colour, fg_options=overlay_samples)
