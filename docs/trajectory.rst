==========
Trajectory
==========

Purpose
=======

The purpose of this document is to capture the direction the application
is heading towards and capture key pivots.

MVP 1
=====

The first MVP is a simple library which enables a declarative style of
configuration in the backend.

The front end will support a simple tile layout, a single view, and a
mechanism for data updates.

The type of data being presented in the first version is:

* Text that can be animated based on state (also supports Font Awesome)
* A simple gauge that both represents a value and can change colour

Completed User Stories:

[x] As a technical user, I can declare a simple tile layout that is rendered on a web dashboard
[x] As a technical user, I want to declare adjustable widths as a proportion of each other
[x] As a technical user, I can declare static text elements to be presented to the dashboard
[x] As a technical user, I can declare dynamic text elements that change text on the dashboard
[x] As a technical user, I can declare a simple gauge that presents a static data value
[x] As a technical user, I can declare a simple gauge that presents a dynamic data value
[x] As a technical user, I can declare a simple gauge that changes colour based on state
[x] As a user I want feedback that the display is up to date or that the system is not working
[x] As a user I want some kind of regularly updating display (like a time field) just to have confidence that data
    is flowing and it has not just crashed.  Make a 5s updating clock that fades between transitions?
[x] B612 Font
[x] Asyncio Views
[x] As a technical user, I want to declare a multiline dynamic text element where individual lines
    can dynamically change state
[x] As a technical user, I want to declare the relative width of each of my gauges on a row
[x] Add a message ability in state that can be a tooltip or touch button that makes a bubble appear
[x] Add icon and tooltip to indicate failed connection (perhaps a standard tile row or fixed on the screen bottom?)

Elements:

[ ] As a technical user, I can declare dynamic text elements that change font properties on the dashboard (size / bold)
[ ] As a technical user, I can configure text to be font awesome icons
[ ] As a technical user, I want to declare that the tiles on my page consume the entire height of my page (or min height of a row)

Style / General UI:

[x] As a user I want unhealthy states that indicate it is bad, but there is nothing I can do about it  (like an BG unhealthy).  This can also be a bit more salient.
[ ] Blank / error controls on data error (500) or server not respond (shutdown server or change ids can cause this)
[ ] Provide some standard gadgets like Server Time vs Client Time, update rates
[ ] Allow non notification information - clicking on the title area to bring up a small message of
    update statistics (maybe count data points received in the last 15 minutes)

System:

[ ] As a user, I want to see application name and version number so that I can see when updates are performed (maybe small on the bottom right?)


Technology
==========

There are some limitations with using Django and DRF (do I really need all that capability?).

It might be worth considering refactoring to use FastAPI, Falcon or similar.  Especially since
DRF does not have asyncio support and most of the dashboard's use case is expected to be IO bound
operations (fetching remote data and responding).

In order to make this understanding, what is the expected use case and required features of a
operational dashboard?

Reviewing the main features Django against dashboard

* Does dashboard need user support?  Maybe some basic user support.  It will eventually need
  complex per view permissions (permissions for different operations).
* Will the dashboard store data:  No.  So models and migrations are not needed.
* It will need good security (SSL, TLS, Client Certs, etc...)
* Management commands?  Probably not needed since there is no data outside of user configuration.
  This means that data migrations might be a thing.
* It will need static file serving (self contained)
* It should only need minimal HTTP serving / templating.  The heavy lifting is expected to be front
  end code that focuses on a declarative approach to dashboard views.
* It would be nice to have a REST API inspector

===========  ==================  ===================  ===============  ===================  ===============
Framework    Permissions?        Static Files?        Security?        Web Pages            REST Inspector?
===========  ==================  ===================  ===============  ===================  ===============
DRF          Yes                 Yes                  Yes              Yes                  Yes
Falcon       No                  Yes?                 Minimal          Maybe                Maybe
FastAPI      No                  Yes?                 Some             Maybe                Yes
===========  ==================  ===================  ===============  ===================  ===============

Notes:

* Falcon's docs is pretty weak.  Falcon does support PyPy
* OpenAPI seems to be the goal for web browsable api
* FastAPI Has some interesting templates:  https://github.com/tiangolo/full-stack-fastapi-postgresql
* Going the low level route (FastAPI) is hard work.

Good JWT References:
* https://www.geeksforgeeks.org/jwt-authentication-with-django-rest-framework/  (server does not store sessions)
* https://en.wikipedia.org/wiki/JSON_Web_Token
* https://www.freecodecamp.org/news/how-to-add-jwt-authentication-in-fastapi/

Reading TODO:
* https://sqlmodel.tiangolo.com/tutorial/fastapi/relationships/

