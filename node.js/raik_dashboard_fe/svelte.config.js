import sveltePreprocess from 'svelte-preprocess'

// noinspection JSUnusedGlobalSymbols
export default {
  // Consult https://github.com/sveltejs/svelte-preprocess
  // for more information about preprocessors
  preprocess: sveltePreprocess()
}
