import {CommunicationError, ServerError} from "../common/errors";

const DEFAULT_LAYOUT_PATH_NAME = "default"  // This is to make searching for usage easier
const END_POINT_VIEW_LAYOUT = `api/layout/${DEFAULT_LAYOUT_PATH_NAME}`
const END_POINT_VIEW_DATA = "api/element"

/**
 * This must match an interface in Python
 */

export enum ElementType {
    Text = "text",
    TextGrid = "text_grid",
    Gauge = "gauge",
}

export enum ElementStates {
    Healthy = "healthy",
    Unhealthy = "unhealthy",
    Info = "info",
    Warning = "warning",
    Error = "error",
    // Critical = "critical",  // There is no use for this currently
}

export const MIN_UPDATE_INTERVAL = 3.0
export const ERROR_PAUSE_INTERVAL = 10.0
export const MAXIMUM_DATA_AGE = 60*1000

export interface TextElementConf {
    no_data: string
    heading: string | null,
    update_interval: number,
}

export interface TextGridElementConf {
    no_data: string
    columns: number
    rows: number
    heading: string | null,
    update_interval: number,
}

export interface GaugeElementConf {
    range_min: number,
    range_max: number,
    units: string,
    decimal_places: number,
    heading: string | null,
    update_interval: number,
}

export interface ElementInterface {
    type: ElementType
    conf: TextElementConf
    data_id: number
    proportion: number
}

export interface TileInterface {
    children: TileInterface[] | ElementInterface[] | null
    vertical: boolean
    proportion: number
}


export interface ElementValueType {
    timestamp: number | null
    value: any | null
    state: string | any[] | object | null
    message: string | any[] | object | null
}



async function fetchResults(url: string, params?: URLSearchParams) {
    params = params ?? new URLSearchParams();
    // params.append("format", "json");
    url = `${url}?${params.toString()}`
    let response;
    try {
        response = await fetch(url);
    } catch {
        throw new CommunicationError("Fetch Error");
    }

    if (response.ok) {
        return await response.json();
    } else {
        const text = await response.text();
        const errorSummary = `Server raised an error code: ${response.status}, see the console for details`;
        console.error(errorSummary);
        console.error(text);
        throw new ServerError(errorSummary);
    }
}

export async function fetchLayout(): Promise<TileInterface> {
    return await fetchResults(END_POINT_VIEW_LAYOUT);
}

export async function fetchElementValue(data_id: number): Promise<ElementValueType>{
    return await fetchResults(`${END_POINT_VIEW_DATA}/${data_id}`);
}