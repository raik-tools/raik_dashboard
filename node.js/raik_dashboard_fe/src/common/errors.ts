// import {errorObject} from "../schedule_view/state/stores";

/**
 * All errors that require user attention should be caught with the
 * appropriate error functions called.
 */

export class UserErrorMessage extends Error {

    constructor(message){
        super(message);
    }
    // noinspection JSUnusedGlobalSymbols
    dashboardErrorTitle = "Error";
    // noinspection JSUnusedGlobalSymbols
    dashboardErrorType = "Error";
}

export class ServerError extends UserErrorMessage {
    constructor(message){
        super(message);
    }
    dashboardErrorTitle = "Server Error";
    dashboardErrorType = "Server";
}


export class CommunicationError extends UserErrorMessage {
    constructor(message){
        super(message);
    }
    dashboardErrorTitle = "Server Communication Error";
    dashboardErrorType = "ServerComm";
}
