/**
 * This file
 */

import {
    ElementStates,
    ERROR_PAUSE_INTERVAL,
    fetchElementValue, MAXIMUM_DATA_AGE,
    MIN_UPDATE_INTERVAL
} from "../../server_interface/serverInterface";
import type {
    ElementValueType,
    TileInterface,
    ElementInterface
} from "../../server_interface/serverInterface";
import {
    setElementError,
    clearElementError,
    setElementMessage,
    clearElementMessage
} from "../state/stores";
import {nowEpochMs, sleep} from "../../common";
import app_style from "../../app_style.module.scss";


export async function elementForeverLoop(conf, data_id: number, cb: (val: ElementValueType) => void) {
    const instance = Math.floor(Math.random() * 10000)
    console.log("Starting Forever Loop", data_id, instance);
    let errorMessageSet = false;

    try{
        // noinspection InfiniteLoopJS
        while ( true ){
            const sleepInterval = Math.max(conf.update_interval, MIN_UPDATE_INTERVAL);
            const val = await fetchElementValue(data_id);

            const dataAge = nowEpochMs() - val.timestamp;
            if ( dataAge > MAXIMUM_DATA_AGE ){
                errorMessageSet = true;
                const hoursAge = Math.floor(dataAge/1000/3600)
                const minutesAge = Math.floor((dataAge-hoursAge*1000*3600)/1000/60)
                const secondsAge = Math.floor((dataAge-hoursAge*1000*3600-minutesAge*1000*60)/1000)

                setElementMessage(data_id, ElementStates.Warning, `Data is ${hoursAge}:${minutesAge}:${secondsAge} old`);
            } else if (errorMessageSet) {
                clearElementMessage(data_id);
                errorMessageSet = false;
                setElementMessage(data_id, val.state, val.message);
            } else {
                setElementMessage(data_id, val.state, val.message);
            }

            try {
                cb(val);
            } catch (err) {
                console.error("There was an error with an element", data_id, err)
                await sleep(ERROR_PAUSE_INTERVAL);
                setElementMessage(data_id, ElementStates.Warning, `Unhandled Exception: ${err}`);
            }

            await sleep(sleepInterval);
        }
    } catch (err){
        if (!err.hasOwnProperty("dashboardErrorType")) {
            throw err;
        }
        clearElementMessage(data_id);
        setElementError(data_id, err.message, err.dashboardErrorType);
        await waitForReconnection(data_id);
        clearElementError(data_id);
    }
}

export async function waitForReconnection(data_id: number) {
    let serverAvailable = false;
    while ( !serverAvailable ){
        try {
        await fetchElementValue(data_id);
        serverAvailable = true;
        } catch (err){
            if (!err.hasOwnProperty("dashboardErrorType")) {
                throw err;
            }
            await sleep(ERROR_PAUSE_INTERVAL);
        }
    }
}


interface ExplicitProportions {
    layout: TileInterface
    proportionClass: string
}


export function layoutHorizontalTileProportions(children: TileInterface[] | ElementInterface[]): ExplicitProportions[] {
    // @ts-ignore
    const totalProportions = children.reduce((a, b) => (a+b.proportion ?? 1), 0);
    return children.map(c => {
        const childProportion = Math.floor((c.proportion ?? 1)/totalProportions * 12);
        const childProportionClass = Math.max(childProportion, 1);
        return {
            layout: c,
            proportionClass: `is-${childProportionClass}`
        }
    });
}


/**
 * This is not implemented yet as it will require a change to flex
 */
export function layoutVerticalTileProportions(children: TileInterface[] | ElementInterface[]): ExplicitProportions[] {
    return children.map(c => ({
        layout: c,
        proportionClass: ""
    }));
}

export function textStateClass(state): string {
    if ( state === ElementStates.Unhealthy) return "has-text-background-darker";
    if ( state === ElementStates.Healthy) return "has-text-background";
    if ( state === ElementStates.Info) return "has-text-info";
    if ( state === ElementStates.Warning) return "has-text-warning";
    if ( state === ElementStates.Error) return "has-text-error";
    return "has-text-background-darker";
}


export const colourMap = {
    "unhealthy": app_style["background-darker"],
    "healthy": app_style.background,
    "info": app_style.info,
    "warning": app_style.warning,
    "error": app_style.error,
}
