import {writable} from 'svelte/store';
import {ElementStates} from "../../server_interface/serverInterface";

/**
 *  When setting the element message the message will automatically appear
 *  for some modes and stay accessible unless the message changes or the user manually
 *  closes the message (performed in the next function).
 *
 */
interface ElementMessage {
    msg: string
    latched: boolean
    clearOnHide: boolean
    display: boolean
    state: ElementStates
}


const initMessageDisplay: {[key: number]: ElementMessage} = {};

export const messageDisplay = writable(initMessageDisplay);

export function setElementMessage(dataId, state, msg) {
    const warningState = state === ElementStates.Warning || state === ElementStates.Error;

    messageDisplay.update(v => {
        const oldMessage = v[dataId]?.msg ?? "";
        const newMessage = msg ?? "";
        const messageLatched = v[dataId]?.latched ?? false;
        let elementMessage;

        if ( newMessage !== "" ) {
            elementMessage = {
                msg: newMessage,
                latched: warningState,
                clearOnHide: false,
                display: oldMessage !== newMessage && warningState,
                state
            };
        }
        else if ( newMessage === "" && messageLatched ) {
            elementMessage = {
                ...v[dataId],
                clearOnHide: true,
            };
        } else {
            elementMessage = undefined;
        }

        v[dataId] = elementMessage;
        return v
    });

}


export function toggleElementMessage(dataId) {
    messageDisplay.update(v => {
        if ( v[dataId] === undefined ){
            // Do Nothing
            return v
        } else {
            let elementMessage;

            if ( v[dataId].clearOnHide ) {
                elementMessage = undefined;
            }
            else {
                elementMessage = {
                    ...v[dataId],
                    latched: false,  // Clear latch when manually closed
                    display: !v[dataId].display
                };
            }
            v[dataId] = elementMessage;
            return v
        }
    })
}


export function clearElementMessage(dataId) {
    messageDisplay.update(v => {
        v[dataId] = undefined;
        return v
    })
}


/**
 *  Track the error of each element
 */
interface ElementError {
    msg: string
    type: string
}

const initElementError: {[key: number]: ElementError | undefined} = {};

export const elementErrorState = writable(initElementError);

export function setElementError(dataId, err: string, type: string) {
    elementErrorState.update(v => {
        v[dataId] = {
            msg: err,
            type: type
        };
        return v
    });

}

export function clearElementError(dataId) {
    elementErrorState.update(v => {
        v[dataId] = undefined;
        return v
    });
}
