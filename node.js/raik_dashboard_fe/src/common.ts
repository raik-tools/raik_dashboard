export function generateId () {
  // https://gist.github.com/gordonbrander/2230317
  return '_' + Math.random().toString(36).substring(2, 9);
}

export function getCookie(name) {
    let cookieValue = null;
    if (document.cookie && document.cookie !== '') {
        const cookies = document.cookie.split(';');
        for (let i = 0; i < cookies.length; i++) {
            const cookie = cookies[i].trim();
            // Does this cookie string begin with the name we want?
            if (cookie.substring(0, name.length + 1) === (name + '=')) {
                cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                break;
            }
        }
    }
    return cookieValue;
}

// https://amiradata.com/javascript-sleep-function/
export const sleep = (seconds) => {
  return new Promise(resolve => setTimeout(resolve, seconds * 1000))
}


export function nowEpochMs() {
  return Date.now()
}
