import {defineConfig} from 'vite'
import {svelte} from '@sveltejs/vite-plugin-svelte'

const root_name = "raik_dashboard_fe.bundle";
const out_root = "../../raik_dashboard/static/raik_dashboard_bundle/"


// https://vitejs.dev/config/
// noinspection JSUnusedGlobalSymbols
export default defineConfig({
    plugins: [
        svelte()
    ],
    build: {
        outDir: out_root,
        lib: {
            entry: "./src/dashboard_view/main.ts",
            name: "raik_dashboard_fe",
            formats: ["iife"],
            fileName: root_name
        },
        sourcemap: true,
    }
})
