import asyncio

import uvicorn
from fastapi import FastAPI
from fastapi.staticfiles import StaticFiles

import raik_dashboard.conf
from raik_dashboard._paths import RAIK_DASHBOARD_STATIC_DIR
from raik_dashboard.api.router import api_router
from raik_dashboard.logic.layouts import add_layout_module
from raik_dashboard.views import router

app = FastAPI(debug=True)

# It seems mount must be on app, not on router (mounting on router fails?)
app.mount(
    "/static/", StaticFiles(directory=str(RAIK_DASHBOARD_STATIC_DIR)), name="static"
)
app.include_router(router)
app.include_router(api_router, prefix="/api")


def serve(
    config: raik_dashboard.conf.Config = raik_dashboard.conf.Config(),
    tasks: list = tuple(),
):
    raik_dashboard.conf.Config.set_config(config)

    for m in config.modules:
        add_layout_module(m)

    uvi_config = uvicorn.Config(
        config.main_app,
        port=config.port,
        host=config.host,
        log_level=config.log_level.lower(),
        proxy_headers=config.proxy_headers,
        forwarded_allow_ips=config.forwarded_allow_ips,
    )
    server = uvicorn.Server(uvi_config)

    async def main():
        for t in tasks:
            asyncio.create_task(t)
        await server.serve()

    asyncio.run(main())


if __name__ == "__main__":

    # async def background_task():
    #     while True:
    #         print("Test  task")
    #         await asyncio.sleep(5.0)

    c = raik_dashboard.conf.Config(log_level="DEBUG")
    # serve(c, tasks=[background_task()])
    serve(c)
