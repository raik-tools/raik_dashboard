# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
import logging
from fastapi import Request
from fastapi.responses import HTMLResponse
from fastapi.templating import Jinja2Templates
from fastapi import APIRouter

from raik_dashboard._paths import RAIK_DASHBOARD_TEMPLATE_DIR

logger = logging.getLogger(__name__)

router = APIRouter()


templates = Jinja2Templates(directory=str(RAIK_DASHBOARD_TEMPLATE_DIR))


@router.get("/", response_class=HTMLResponse)
def homepage(request: Request):
    # There is an issue whe the Host header is not being changed by
    # NGINX or at least something in between is reverting it back to
    # the proxy host address.  This causes static files to fail,
    # as such, the template below is hard coding static file addresses.
    # logger.warning("DEBUG Request Scope:  %s", request.scope)
    return templates.TemplateResponse(
        "raik_dashboard/frontend.html", {"request": request}
    )
