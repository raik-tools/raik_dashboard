# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.
"""
There are a number of functions related to the models that just do not
seem to fit well with the model classed.  I prefer that a model class
focuses on modelling the behaviour or entity that the model is designed
around, not just being an arbitrary namespace.  This is more than just
an MVC type application as there is background functionality and events
That need processing on the backend.

This is the home of that logic.
"""
