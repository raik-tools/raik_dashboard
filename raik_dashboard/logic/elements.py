"""
Management and retrieval of elements
"""
from typing import Dict
from raik_dashboard.layouts.base_element import BaseElement


def get_elements() -> Dict[int, BaseElement]:
    return BaseElement.get_elements()
