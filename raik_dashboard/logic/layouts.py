"""
Management and retrieval of layouts
"""
import functools
import importlib
from typing import Dict

from raik_dashboard.conf import Config
from raik_dashboard.layouts import Tile

DEFAULT_LAYOUT_PATH_NAME = "default"

_layouts = {}


def add_layout_module(modpath: str):
    """
    Add the import path to a layout module, such as "raik_dashboard.layout"
    """
    global _layouts

    if "." in modpath:
        name_space = modpath.split(".")[0]
    else:
        name_space = None

    module = importlib.import_module(modpath)
    for obj_name in dir(module):
        if obj_name[0] != "_":
            layout_key = f"{name_space}.{obj_name}" if name_space else obj_name
            o = getattr(module, obj_name)
            if isinstance(o, Tile):
                _layouts[layout_key] = o


def get_layouts() -> Dict[str, callable]:
    global _layouts
    return _layouts


@functools.cache
def get_default_layout() -> str:
    global _layouts
    default_layout = Config.get_config().default_layout
    if default_layout is not None:
        # noinspection PyTypeChecker
        return default_layout
    else:
        return next(iter(_layouts))
