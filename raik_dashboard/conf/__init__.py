import functools
import logging.config
import os
import pathlib
import re

from raik_history.time_and_calendar import ENV_VAR_TIME_ZONE


@functools.cache
def read_env_vars_defaults(env_var_path: pathlib.Path) -> dict[str, str]:
    """
    Loads the default value listed in an env.vars file
    """
    if env_var_path.exists():
        val_match = re.compile(
            r"^export (?P<key>[A-Z_]+)=\${[A-Z_]+:-(?P<val>.*)}", re.MULTILINE
        )
        with env_var_path.open("r") as f:
            text = f.read()

        _vars = {x["key"]: x["val"] for x in val_match.finditer(text)}
        return _vars
    else:
        return {}


class Config:

    _current_config = None

    class EnvKeys:
        secret_key = "SECRET_KEY"
        log_level = "LOG_LEVEL"
        debug = "DEBUG"
        remote_debug = "REMOTE_DEBUG"
        default_layout = "DEFAULT_DASHBOARD_LAYOUT"
        port = "DASHBOARD_PORT"
        host = "DASHBOARD_HOST"
        main_app = "DASHBOARD_APP"
        modules = "DASHBOARD_MODULES"
        proxy_headers = "PROXY_HEADERS"
        forwarded_allow_ips = "FORWARDED_ALLOW_IPS"
        time_zone = ENV_VAR_TIME_ZONE

    @classmethod
    def set_config(cls, c: "Config"):
        if cls._current_config is not None:
            raise ValueError(
                "RAIK Dashboard configuration has already been set, runtime changes "
                "are not supported"
            )
        else:
            cls._current_config = c

    @classmethod
    def get_config(cls) -> "Config":
        if cls._current_config is None:
            raise ValueError("RAIK Dashboard configuration has not been set")
        else:
            return cls._current_config

    def __init__(
        self,
        env_vars_path: pathlib.Path = pathlib.Path("./conf/env.vars"),
        var_path: pathlib.Path = pathlib.Path("./var"),
        port: int | None = None,
        host: str | None = None,
        main_app: str | None = None,
        modules: str | None = None,
        secret_key: str | None = None,
        log_level: str | None = None,
        debug: str | None = None,
        remote_debug: str | None = None,
        default_layout: str = None,
        time_zone: str = None,
        proxy_headers: bool = None,
        forwarded_allow_ips: str = None
        # Future vars:
        # allowed_hosts,  pagination vars, cache location, language_code, time_zone
    ):
        if env_vars_path and env_vars_path.exists():
            self._env_var_defaults = read_env_vars_defaults(pathlib.Path(env_vars_path))
        else:
            self._env_var_defaults = {}
        self.port = int(self.find_config_value(self.EnvKeys.port, port, 8080))
        self.host = self.find_config_value(self.EnvKeys.host, host, "127.0.0.1")
        self.main_app = self.find_config_value(
            self.EnvKeys.main_app, main_app, "raik_dashboard.main:app"
        )
        self.modules = self.find_config_value(
            self.EnvKeys.modules, modules, "raik_dashboard.layouts"
        ).split(",")

        self.secret_key = self.check_value_is_file(
            self.find_config_value(self.EnvKeys.secret_key, secret_key, None)
        )
        self.log_level = self.find_config_value(
            self.EnvKeys.log_level, log_level, "INFO"
        )
        self.debug = self.find_bool_config_value(self.EnvKeys.debug, debug, None)
        self.remote_debug = self.find_bool_config_value(
            self.EnvKeys.remote_debug, remote_debug, None
        )
        self.default_layout = self.find_config_value(
            self.EnvKeys.default_layout, default_layout, "raik_dashboard.sample_layout"
        )
        self.time_zone = self.find_config_value(
            self.EnvKeys.time_zone, time_zone, "Australia/Brisbane"
        )
        self.proxy_headers = self.find_config_value(
            self.EnvKeys.proxy_headers, proxy_headers, False
        )
        self.forwarded_allow_ips = self.find_config_value(
            self.EnvKeys.forwarded_allow_ips, forwarded_allow_ips, "127.0.0.1"
        )

        # if REMOTE_DEBUG:
        #     import pydevd_pycharm
        #
        #     pydevd_pycharm.settrace(
        #         "localhost", port=51234, stdoutToServer=True, stderrToServer=True, suspend=True
        #     )

        self.var_path = var_path
        logging.config.dictConfig(self.default_logging_config())

        # Ensure Timezone is set
        os.environ[ENV_VAR_TIME_ZONE] = self.time_zone

    def find_config_value(self, key, init_val, default=None):
        # noinspection GrazieInspection
        """
        Finds the configuration value based on the following order
        of precedence:  init value (if set),  environment variable, env.vars file
        """
        if init_val is not None:
            return init_val
        elif os.environ.get(key, None) is not None:
            return os.environ.get(key)
        elif self._env_var_defaults.get(key, None) is not None:
            return self._env_var_defaults.get(key)
        else:
            return default

    def find_bool_config_value(self, key, init_val, default=None):
        return self.find_config_value(key, init_val, default) in (
            "TRUE",
            "True",
            "true",
            "1",
        )

    @staticmethod
    def check_value_is_file(v):
        """
        If the value provided looks like a file path, load the variable from the file
        """
        if v and (v[0] == "/" or v[:2] == "./"):
            with open(v) as f:
                return f.read().strip()
        else:
            return v

    def default_logging_config(self):
        log_path = self.var_path / "log"

        log_conf = {
            "version": 1,
            "disable_existing_loggers": False,
            "formatters": {
                "verbose": {
                    "format": "{levelname} {asctime} {name} {process:d} {thread:d} {message}",
                    "style": "{",
                },
                "simple": {
                    "format": "{levelname} {asctime} {name} {message}",
                    "style": "{",
                },
            },
            "handlers": {
                "console": {
                    # 'filters': ['require_debug_true'],
                    "class": "logging.StreamHandler",
                    "formatter": "simple",
                    "level": self.log_level,
                }
            },
            "loggers": {
                "asyncio": {
                    "handlers": ["console"],
                    "propagate": False,
                    "level": "INFO",
                },
            },
        }

        if log_path.exists():
            log_conf["handlers"]["app_log"] = {
                "class": "logging.FileHandler",
                "filename": log_path / "app.log",
                "formatter": "verbose",
                "level": "INFO",
            }
            log_conf["handlers"]["debug_log"] = {
                "class": "logging.FileHandler",
                "filename": log_path / "debug.log",
                "formatter": "verbose",
                "level": "DEBUG",
            }
            log_conf["root"] = {
                "handlers": ["console", "app_log", "debug_log"]
                if self.log_level == "DEBUG"
                else ["console", "app_log"],
                "level": self.log_level,
            }
        else:
            log_conf["root"] = {"handlers": ["console"], "level": self.log_level}

        return log_conf
