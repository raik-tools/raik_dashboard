import secrets


def get_random_security_string():
    """ """
    chars = "abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)"
    length = 50
    return "".join(secrets.choice(chars) for _ in range(length))
