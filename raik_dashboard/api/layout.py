# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from fastapi import APIRouter

from raik_dashboard.api.schemas.layouts import LayoutList
from raik_dashboard.logic.layouts import (
    get_layouts,
    get_default_layout,
    DEFAULT_LAYOUT_PATH_NAME,
)

router = APIRouter()


@router.get("/", response_model=LayoutList)
async def list_layouts():
    layouts = [
        {
            "name": layout_ref,
            "url": router.url_path_for("get_layout", layout_name=layout_ref),
        }
        for layout_ref in get_layouts()
    ]
    return {"count": len(layouts), "data": layouts}


@router.get("/{layout_name}")
async def get_layout(layout_name: str):
    if layout_name == DEFAULT_LAYOUT_PATH_NAME:
        layout_name = get_default_layout()
    return get_layouts()[layout_name]()
