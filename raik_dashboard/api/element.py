# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: GPL-3.0-or-later
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License,
# or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
# See the GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <https://www.gnu.org/licenses/>.

from fastapi import APIRouter

from raik_dashboard.logic.elements import get_elements
from raik_dashboard.api.schemas.elements import ElementList

router = APIRouter()


@router.get("/", response_model=ElementList)
async def list_elements():
    elements = [
        {
            "data_id": d_id,
            "url": router.url_path_for("get_element_value", data_id=d_id),
        }
        for d_id in get_elements()
    ]

    return {"count": len(elements), "data": elements}


@router.get("/{data_id}")
async def get_element_value(data_id: int):
    elements = get_elements()
    return await elements[data_id].get_data()
