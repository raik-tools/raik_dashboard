from pydantic import BaseModel


class Layout(BaseModel):
    name: str = None
    url: str = None


class LayoutList(BaseModel):
    count: int = 0
    data: list[Layout] = []
