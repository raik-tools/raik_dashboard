from typing import TypedDict

from pydantic import BaseModel
from raik_history.series.constants import Timestamp, Value


class Element(BaseModel):
    data_id: int = None
    url: str = None


class ElementList(BaseModel):
    count: int = 0
    data: list[Element] = []


ValueTypes = Value | list[Value] | list[list[Value]] | dict[str, Value]


class ElementValueType(TypedDict):
    timestamp: Timestamp
    value: ValueTypes
    state: str | list | dict  # Can be a string or a structure of strings
    message: str | list[str] | None
