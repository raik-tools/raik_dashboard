from fastapi import APIRouter

import raik_dashboard.api.element
import raik_dashboard.api.layout

api_router = APIRouter()
api_router.include_router(
    raik_dashboard.api.layout.router, prefix="/layout", tags=["layout"]
)
api_router.include_router(
    raik_dashboard.api.element.router, prefix="/element", tags=["element"]
)
