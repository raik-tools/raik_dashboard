Ideally all vendor assets will be compiled into the bundle by VITE,
however, not all vendor packages can be self-contained this way.  For
example, Font Awesome font libraries are not presently able to be
bundled.