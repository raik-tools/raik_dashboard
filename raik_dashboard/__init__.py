import pathlib
import re


def _get_version():
    project_file = pathlib.Path(__file__).parent.parent / "pyproject.toml"
    if project_file.exists():
        with project_file.open("r") as f:
            content = f.read()
        version_match = next(
            re.finditer(r"version = \"(?P<v>.+)\"\n", content)
        ).groupdict()["v"]
        return version_match
    else:
        import importlib.metadata

        return importlib.metadata.version("raik_dashboard")


__version__ = _get_version()
