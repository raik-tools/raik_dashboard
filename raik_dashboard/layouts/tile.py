class BaseTile:
    """
    Captures configuration common to both Tiles and Elements in terms of layout
    """

    def __init__(self, proportion=1):
        self.proportion = proportion

    def as_json_serializable(self):
        return {
            "proportion": self.proportion,
        }


class Tile(BaseTile):
    """
    This is a base class used to declare a dashboard Tile Structure.

    As dashboard structure is expected to be static, classes based on this
    are expected to be statically defined and then will resolve as json serializable
    data to be sent to the front end.

    Note that args is a list of child object and this list must contain the same
    type, either all tiles or all elements.
    """

    def __init__(self, *args, vertical=False, proportion=1):
        self.children = args
        self.vertical = vertical
        super(Tile, self).__init__(proportion=proportion)

    def __call__(self, *args, **kwargs):
        return self.as_json_serializable()

    def as_json_serializable(self):
        base_conf = super(Tile, self).as_json_serializable()
        base_conf.update(
            {
                "vertical": self.vertical,
                "children": [c.as_json_serializable() for c in self.children],
            }
        )
        return base_conf
