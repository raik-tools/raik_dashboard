import random
from typing import Optional, Any

from raik_history.quality import QualityCodes
from raik_history.time_and_calendar import now_epoch_ms

from raik_dashboard.layouts.base_element import (
    BaseElement,
    CompoundValue,
    CompoundState,
    ElementStates,
)


class TextGridElement(BaseElement):
    """
    A grid of dynamic text items displayed on the screen.
    """

    type = "text_grid"
    no_data: str = "-"
    columns: int = 2
    rows: int = 3

    def __init__(
        self,
        no_data: Optional[str] = None,
        columns: Optional[int] = None,
        rows: Optional[int] = None,
        **kwargs
    ):
        self.no_data = self.class_or_init_attr("no_data", no_data)
        self.columns = self.class_or_init_attr("columns", columns)
        self.rows = self.class_or_init_attr("rows", rows)
        super(TextGridElement, self).__init__(**kwargs)

    def get_conf(self):
        conf = super(TextGridElement, self).get_conf()
        conf["no_data"] = self.no_data
        conf["columns"] = self.columns
        conf["rows"] = self.rows
        return conf

    async def get_state(self, value: CompoundValue) -> CompoundState:
        # noinspection PyTypeChecker
        return [
            [ElementStates.Info.value for _ in range(self.columns)]
            for __ in range(self.rows)
        ]

    async def get_value(self) -> Any:
        return (
            now_epoch_ms(),
            [[self.no_data for _ in range(self.columns)] for __ in range(self.rows)],
            QualityCodes.GOOD_NON_SPECIFIC,
        )


class RandomTextGridElement(TextGridElement):
    """
    Used for testing and demonstration
    """

    word_list = (
        "time person year way day thing man world life hand "
        "part child eye woman place work".split(" ")
    )

    async def get_state(self, value: CompoundValue) -> CompoundState:
        # noinspection PyTypeChecker
        return [
            [
                random.choice(list(ElementStates)).value,
                random.choice(list(ElementStates)).value,
            ],
            [
                random.choice(list(ElementStates)).value,
                random.choice(list(ElementStates)).value,
            ],
            [
                random.choice(list(ElementStates)).value,
                random.choice(list(ElementStates)).value,
            ],
        ]

    async def get_value(self) -> Any:
        return (
            now_epoch_ms(),
            [
                [random.choice(self.word_list), random.choice(self.word_list)],
                [random.choice(self.word_list), random.choice(self.word_list)],
                [random.choice(self.word_list), random.choice(self.word_list)],
            ],
            QualityCodes.GOOD_NON_SPECIFIC,
        )
