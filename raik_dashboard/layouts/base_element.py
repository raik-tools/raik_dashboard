import abc
from enum import Enum
from typing import Optional, Any

from raik_history.series.constants import DataTuple, Timestamp, Quality

from raik_dashboard.api.schemas.elements import ElementValueType, ValueTypes
from raik_dashboard.layouts.tile import BaseTile


class ElementStates(Enum):
    """
    While states should capture severity, it is more important that
    they capture how quickly a user needs to take action in the
    current context.

    For example, there is no point having an alarm to say your
    industrial plant has exploded... by the time the explosion
    has occurred, it is too late to take action (and it is usually
    pretty obvious after a catastrophic failure has occurred).

    From a situational awareness perspective, the aim is to have
    states that are the lowest possible state and preserve more
    severe states for situations that are actually severe.

    Here are some guidelines:

    :cvar Healthy:  The default to say everything is okay and no actions
                    are needed.
    :cvar Unhealthy:  Something is not okay, and no action is needed.
    :cvar Info:  Some action might be needed in the future (today sometime)
    :cvar Warning:  Some action will be needed soon if the system does not
                    get better.
    :cvar Error:  Action is needed now
    """

    Healthy = "healthy"
    Unhealthy = "unhealthy"
    Info = "info"
    Warning = "warning"
    Error = "error"
    # Critical = "critical", // There is no use for this currently


CompoundValue = DataTuple | tuple[Timestamp, ValueTypes, Quality]

CompoundState = ElementStates | list[str] | list[list[str]] | dict[str, str]


class BaseElement(BaseTile):
    """
    This is a base class used to declare widgets on a dashboard
    """

    type = None
    _global_ids = {}
    _instantiation_order = 0

    update_interval: float = 5.0
    heading: Optional[str] = None

    def class_or_init_attr(self, attr_name: str, init_val):
        """
        This is a utility function.  If init_val is None then the class
        value is returned (if defined).  Otherwise, the init value is returned.

        This is to allow for static or dynamic variable initialisation.

        It also provides explicit description on how the variables are set.
        """
        if getattr(self, attr_name, None) is not None and init_val is None:
            return getattr(self, attr_name)
        else:
            return init_val

    def __init__(
        self,
        update_interval: Optional[float] = None,
        heading: Optional[str] = None,
        proportion=1,
    ):
        self.update_interval = self.class_or_init_attr(
            "update_interval", update_interval
        )
        self.heading = self.class_or_init_attr("heading", heading)
        self.raw_value: Optional[Any] = None
        self.value: Optional[CompoundValue] = None
        self.state: Optional[CompoundState] = None
        self.message: Optional[str | list[str]] = None

        super(BaseElement, self).__init__(proportion=proportion)

        # Update global references to this instance
        BaseElement._instantiation_order += 1
        self._id = BaseElement._instantiation_order
        BaseElement._global_ids[self.id] = self

    @property
    def id(self) -> int:
        """
        The ID value aims (but does not guarantee) to be stable between
        process restarts.  As long as layout configuration does not change
        then most of the time the id value should be constant.
        """
        return self._id

    @classmethod
    def get_elements(cls) -> dict[int, "BaseElement"]:
        return cls._global_ids

    def get_conf(self) -> dict:
        """
        This must be set in each child and return a json friendly structure.
        This should not be blocking or resource demanding in any way.
        """
        return {
            "heading": self.heading,
            "update_interval": self.update_interval,
        }

    @abc.abstractmethod
    async def get_value(self) -> Any:
        """
        Returns the value for the element to display.

        This is to be derived by child classes to represent the value of the element
        """

    # noinspection PyMethodMayBeStatic
    def clean_value(self, value: Any) -> CompoundValue:
        """
        Returns the value for the element to display.

        This is to be derived by child classes to represent the value of the element
        """
        return value

    async def get_state(self, value: CompoundValue) -> CompoundState:
        """
        Returns the state of the element (or parts thereof)

        This is to be derived by child classes to represent the state of the element
        """
        return ElementStates.Info

    # noinspection PyMethodMayBeStatic
    async def get_message(
        self, value: CompoundValue, state: CompoundState
    ) -> str | list[str] | None:
        """
        Returns a message to display the user to correspond with the state.

        This is to be derived by child classes to represent the state of the element
        """
        return None

    async def get_data(self) -> ElementValueType:
        """
        Returns the value send to the gauge on regular polls.
        """
        self.raw_value = None
        self.value = None
        self.state = None
        self.message = None

        self.raw_value = await self.get_value()
        self.value = self.clean_value(self.raw_value)
        self.state = await self.get_state(self.value)
        self.message = await self.get_message(self.value, self.state)
        if isinstance(self.state, ElementStates):
            s = self.state.value
        else:
            s = self.state

        return ElementValueType(
            timestamp=self.value[0],
            value=self.value[1],
            state=s,
            message=self.message,
        )

    def as_json_serializable(self):
        """
        Returns this classes data for serialization
        """
        base_conf = super(BaseElement, self).as_json_serializable()
        base_conf.update(
            {"type": self.type, "conf": self.get_conf(), "data_id": self.id}
        )
        return base_conf
