"""
This module is used to store hard-coded layouts that are rendered by
the front end.
"""
from raik_history.units_of_measure import U

from raik_dashboard.layouts.gauge_element import RandomGaugeElement, GaugeElement
from raik_dashboard.layouts.text_grid_element import RandomTextGridElement
from raik_dashboard.layouts.tile import Tile
from raik_dashboard.layouts.text_element import (
    RandomTextElement,
    TextElement,
    ClockElement,
)

sample_layout = Tile(
    Tile(
        Tile(
            RandomTextElement("First", heading="Heading"),
            RandomTextElement("Second"),
            vertical=True,
        ),
        Tile(RandomTextElement("Third"), RandomTextElement("Fourth")),
    ),
    Tile(RandomTextElement("fifth"), RandomTextElement("sixth")),
    Tile(
        RandomGaugeElement(
            units=U.percent, decimal_places=0, heading="Another Heading"
        ),
        RandomGaugeElement(units=U.percent, decimal_places=1),
        RandomGaugeElement(
            units=U.kilowatts, decimal_places=2, range_min=-5, range_max=5
        ),
        RandomGaugeElement(
            units=U.kilowatts, decimal_places=2, range_min=0, range_max=25
        ),
    ),
    Tile(ClockElement(), RandomTextGridElement(proportion=3)),
    vertical=True,
)
