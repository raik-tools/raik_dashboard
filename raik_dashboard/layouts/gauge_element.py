from typing import Optional, Any
import random
import pint
from raik_history.quality import QualityCodes
from raik_history.time_and_calendar import now_epoch_ms
from raik_dashboard.layouts.text_element import RandomTextElement
from raik_dashboard.layouts.base_element import (
    BaseElement,
    CompoundValue,
    CompoundState,
    ElementStates,
)


class GaugeElement(BaseElement):
    """
    A simple gauge element
    """

    type = "gauge"

    range_min: float = 0.0
    range_max: float = 100.0
    units: pint.Unit = None
    decimal_places: int = 2

    def __init__(
        self,
        range_min: Optional[float] = None,
        range_max: Optional[float] = None,
        units: pint.Unit = None,
        decimal_places: Optional[int] = None,
        **kwargs,
    ):
        self.range_min = self.class_or_init_attr("range_min", range_min)
        self.range_max = self.class_or_init_attr("range_max", range_max)
        self.units = self.class_or_init_attr("units", units)
        self.decimal_places = self.class_or_init_attr("decimal_places", decimal_places)
        super(GaugeElement, self).__init__(**kwargs)

    def get_conf(self):
        conf = super(GaugeElement, self).get_conf()
        conf["range_min"] = self.range_min
        conf["range_max"] = self.range_max
        conf["units"] = f"{self.units:~}" if self.units else ""
        conf["decimal_places"] = self.decimal_places
        return conf

    async def get_value(self) -> Any:
        """
        This is to be derived by child classes to rep
        """
        return now_epoch_ms(), None, QualityCodes.GOOD_NON_SPECIFIC


class RandomGaugeElement(GaugeElement):
    """
    Used for testing and demonstration
    """

    async def get_state(self, value: CompoundValue) -> CompoundState:
        return random.choice(list(ElementStates))

    async def get_value(self) -> Any:
        r = self.range_max - self.range_min
        v = self.range_min + random.random() * r
        return now_epoch_ms(), v, QualityCodes.GOOD_NON_SPECIFIC

    async def get_message(
        self, value: CompoundValue, state: CompoundState
    ) -> str | list[str] | None:
        # noinspection PyTypeChecker
        return await RandomTextElement.get_message(self, value, state)
