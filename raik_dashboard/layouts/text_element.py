import random
from typing import Optional, Any

from raik_history.quality import QualityCodes
from raik_history.time_and_calendar import now_epoch_ms, now_datetime

from raik_dashboard.layouts.base_element import (
    BaseElement,
    CompoundValue,
    CompoundState,
    ElementStates,
)


class TextElement(BaseElement):
    """
    A text on the screen that can animate by changing colour
    """

    type = "text"
    no_data: str = "-"

    def __init__(self, no_data: Optional[str] = None, **kwargs):
        self.no_data = self.class_or_init_attr("no_data", no_data)
        super(TextElement, self).__init__(**kwargs)

    def get_conf(self):
        conf = super(TextElement, self).get_conf()
        conf["no_data"] = self.no_data
        return conf

    async def get_value(self) -> Any:
        """
        This is to be derived by child classes to rep
        """
        return now_epoch_ms(), self.no_data, QualityCodes.GOOD_NON_SPECIFIC


class RandomTextElement(TextElement):
    """
    Used for testing and demonstration
    """

    word_list = (
        "time person year way day thing man world life hand "
        "part child eye woman place work".split(" ")
    )

    async def get_state(self, value: CompoundValue) -> CompoundState:
        return random.choice(list(ElementStates))

    async def get_value(self) -> Any:
        v = random.choice(self.word_list)
        return now_epoch_ms(), v, QualityCodes.GOOD_NON_SPECIFIC

    async def get_message(
        self, value: CompoundValue, state: CompoundState
    ) -> str | list[str] | None:
        if state != ElementStates.Healthy:
            return random.choice(RandomTextElement.word_list)
        else:
            return None


class ClockElement(TextElement):
    """
    Provides a local time from the server
    """

    update_interval = 5.0

    async def get_state(self, value: CompoundValue) -> CompoundState:
        return ElementStates.Healthy

    async def get_value(self) -> Any:
        v = now_datetime()
        return (
            now_epoch_ms(),
            f"{v.hour:02}:{v.minute:02}:{v.second:02}",
            QualityCodes.GOOD_NON_SPECIFIC,
        )
