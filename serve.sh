#!/bin/bash
# Copyright: (c) 2022, Gary Thompson <coding@garythompson.au>
# SPDX-License-Identifier: MIT

poetry run python raik_dashboard/main.py
