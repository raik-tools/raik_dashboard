==============
RAIK Dashboard
==============

Introduction
============

There are many dashboard solutions available, most will be more
sophisticated than this.  The purpose of this dashboard server is
simplicity and ease of use for someone with some coding capability.  It
will not be a front end configurable tool.  Instead it allows for a
backend declaration of a dashboard (or series of dashboards) that are
rendered in the front end while the backend is responsible for
aggregating all the data sources for the dashboard.

Limitations
===========

This is still very much in its early stages.

Note that while this project includes a django site and associated files,
the actual distributable is just the raik_dashboard module.  Everything else
is just used as a demonstration of how to implement a dashboard project.


Documentation
=============

The documentation can be explored in the docs folder.  This also includes
my own thoughts and project planning.  Unfortunately, I do not have any
useful how-to information as I plan to do this once I built a better UI.

See Also:

https://www.uvicorn.org/deployment/#running-behind-nginx


License
=======

GPL-3.0-or-later
